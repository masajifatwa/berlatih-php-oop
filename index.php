<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';
$sheep = new Animal("shaun",2,"false");

echo $sheep->name."<br>"; // "shaun"
echo $sheep->legs."<br>"; // 2
echo $sheep->cold_blooded."<br>"."<br>"; // false

$sungokong = new Ape("kera sakti",2);
echo $sungokong->name."<br>";
echo $sungokong->legs."<br>";
$sungokong->yell()."<br>"; // "Auooo"
echo "<br><br>";
$kodok = new Frog("buduk",4);
echo $kodok->name."<br>";
echo $kodok->legs."<br>";
$kodok->jump() ;
?>